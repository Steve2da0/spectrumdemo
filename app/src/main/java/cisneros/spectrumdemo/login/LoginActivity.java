package cisneros.spectrumdemo.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import cisneros.spectrumdemo.R;
import cisneros.spectrumdemo.mainActivity.MainActivity;
import cisneros.spectrumdemo.newUser.NewUserActivity;

public class LoginActivity extends AppCompatActivity implements LoginActivityUpdater {

    private LoginActivityPresenter presenter;
    private TextInputLayout usernameLayout;
    private TextInputLayout passwordLayout;
    private MaterialButton loginButton;
    private MaterialButton signupButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);

        this.presenter = new LoginActivityPresenter(this);
        this.usernameLayout = findViewById(R.id.username_layout);
        this.passwordLayout = findViewById(R.id.password_layout);
        this.loginButton = findViewById(R.id.loginButton);
        this.signupButton = findViewById(R.id.signup_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideErrors();
                String username = usernameLayout.getEditText().getText().toString().trim();
                String password = passwordLayout.getEditText().getText().toString().trim();
                presenter.loginButtonClicked(username, password);
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideErrors();
                presenter.signUpButtonClicked();
            }
        });
    }

    @Override
    public void launchMainActivity() {
        Intent mainActivtyIntent = new Intent(this, MainActivity.class);
        startActivity(mainActivtyIntent);
    }

    @Override
    public void launchNewUserActivity() {
        Intent newUserActivity = new Intent(this, NewUserActivity.class);
        newUserActivity.putExtra("LaunchMainActivity", true);
        startActivity(newUserActivity);
    }

    private void hideErrors() {
        this.usernameLayout.setErrorEnabled(false);
        this.passwordLayout.setErrorEnabled(false);
    }

    @Override
    public void displayUsernameError(int errorMsg) {
        this.usernameLayout.setError(getString(errorMsg));
        this.usernameLayout.setErrorEnabled(true);
    }

    @Override
    public void displayPasswordError(int errorMsg) {
        this.passwordLayout.setError(getString(errorMsg));
        this.passwordLayout.setErrorEnabled(true);
    }

    @Override
    public void displaySnackbar(int errorMsg, int length) {
        Snackbar.make(getCurrentFocus(), errorMsg, length).show();
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }
}
