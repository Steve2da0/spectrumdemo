package cisneros.spectrumdemo.newUser;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import cisneros.spectrumdemo.R;
import cisneros.spectrumdemo.mainActivity.MainActivity;

public class NewUserActivity extends AppCompatActivity implements NewUserActivityUpdater {

    private NewUserActivityPresenter presenter;
    private TextInputLayout usernameTextLayout;
    private TextInputLayout passwordTextLayout;
    private TextInputLayout firstNameTextLayout;
    private TextInputLayout lastNameTextLayout;
    private TextInputLayout phoneNumberTextLayout;
    private TextInputLayout emailTextLayout;
    private MaterialButton submitButton;
    private Boolean launchMainActivity = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_user_page);

        Intent intent = getIntent();
        launchMainActivity = intent.getBooleanExtra("LaunchMainActivity", false);
        this.presenter = new NewUserActivityPresenter(this);
        this.usernameTextLayout = findViewById(R.id.username_layout);
        this.passwordTextLayout = findViewById(R.id.password_layout);
        this.firstNameTextLayout = findViewById(R.id.firstname_layout);
        this.lastNameTextLayout = findViewById(R.id.lastName_layout);
        this.phoneNumberTextLayout = findViewById(R.id.phonenumber_layout);
        this.emailTextLayout = findViewById(R.id.email_layout);
        this.submitButton = findViewById(R.id.submit_button);
        this.submitButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hideErrors();

                String username = usernameTextLayout.getEditText().getText().toString().trim();
                String password = passwordTextLayout.getEditText().getText().toString().trim();
                String firstName = firstNameTextLayout.getEditText().getText().toString().trim();
                String lastName = lastNameTextLayout.getEditText().getText().toString().trim();
                String phoneNumber = phoneNumberTextLayout.getEditText().getText().toString();
                String email = emailTextLayout.getEditText().getText().toString().trim();

                presenter.submitButtonClicked(username, password, firstName, lastName, phoneNumber, email);
            }
        });
    }

    private void hideErrors() {
        this.usernameTextLayout.setErrorEnabled(false);
        this.passwordTextLayout.setErrorEnabled(false);
        this.firstNameTextLayout.setErrorEnabled(false);
        this.lastNameTextLayout.setErrorEnabled(false);
        this.phoneNumberTextLayout.setErrorEnabled(false);
        this.emailTextLayout.setErrorEnabled(false);
    }

    @Override
    public void killActivity() {
        if(launchMainActivity) {
            Intent mainActivityIntent = new Intent(this, MainActivity.class);
            startActivity(mainActivityIntent);
        }

        finish();
    }

    @Override
    public void displayUsernameError(int errorMsg) {
        this.usernameTextLayout.setError(getString(errorMsg));
        this.usernameTextLayout.setErrorEnabled(true);
    }

    @Override
    public void displayPasswordError(int errorMsg) {
        this.passwordTextLayout.setError(getString(errorMsg));
        this.passwordTextLayout.setErrorEnabled(true);
    }

    @Override
    public void displayFirstNameError(int errorMsg) {
        this.firstNameTextLayout.setError(getString(errorMsg));
        this.firstNameTextLayout.setErrorEnabled(true);
    }

    @Override
    public void displayLastNameError(int errorMsg) {
        this.lastNameTextLayout.setError(getString(errorMsg));
        this.lastNameTextLayout.setErrorEnabled(true);
    }

    @Override
    public void displayPhoneNumberError(int errorMsg) {
        this.phoneNumberTextLayout.setError(getString(errorMsg));
        this.phoneNumberTextLayout.setErrorEnabled(true);
    }

    @Override
    public void displayEmailError(int errorMsg) {
        this.emailTextLayout.setError(getString(errorMsg));
        this.emailTextLayout.setErrorEnabled(true);
    }

    @Override
    public void displaySnackbarMessage(int errorMsg, int length) {
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }
}
