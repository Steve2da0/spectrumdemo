package cisneros.spectrumdemo.backend;

import java.util.ArrayList;

public interface DatabaseCallback {

    /**
     * * Retrieves all of the users in the database
     * @return - the list of users in the database.
     */
    ArrayList<User> onUserRetrievalSuccess();

    /**
     * Call this whenever an error occurs
     */
    void onFail();

}
