package cisneros.spectrumdemo.mainActivity;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.textview.MaterialTextView;
import cisneros.spectrumdemo.R;

public class UserRecyclerViewHolder extends RecyclerView.ViewHolder implements UserItemUpdater {

    private MaterialTextView usernameText;
    private MaterialTextView firstnameText;
    private MaterialTextView lastnameText;
    private MaterialTextView phonenumberText;
    private MaterialTextView emailText;

    public UserRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        this.usernameText = itemView.findViewById(R.id.username);
        this.firstnameText = itemView.findViewById(R.id.first_name);
        this.lastnameText = itemView.findViewById(R.id.last_name);
        this.phonenumberText = itemView.findViewById(R.id.phone_number);
        this.emailText = itemView.findViewById(R.id.user_email);
    }

    @Override
    public void setUsername(String username) {
        this.usernameText.setText(username);
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstnameText.setText(firstName);
    }

    @Override
    public void setLastName(String lastName) {
        this.lastnameText.setText(lastName);
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.phonenumberText.setText(phoneNumber);
    }

    @Override
    public void setEmail(String email) {
        this.emailText.setText(email);
    }
}
