package cisneros.spectrumdemo.login;

import android.content.Context;

public interface LoginActivityUpdater {

    void launchMainActivity();

    void launchNewUserActivity();

    void displayUsernameError(int errorMsg);

    void displayPasswordError(int errorMsg);

    void displaySnackbar(int errorMsg, int length);

    Context getContext();
}
