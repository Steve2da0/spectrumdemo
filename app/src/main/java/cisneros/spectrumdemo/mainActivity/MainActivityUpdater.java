package cisneros.spectrumdemo.mainActivity;

import android.content.Context;

import java.util.ArrayList;

import cisneros.spectrumdemo.backend.User;

public interface MainActivityUpdater {

    void displayProgressBar();

    void hideProgressBar();

    void updateUsers(ArrayList<User> users);

    Context getContext();

}
