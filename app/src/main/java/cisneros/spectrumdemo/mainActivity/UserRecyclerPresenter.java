package cisneros.spectrumdemo.mainActivity;

import java.util.ArrayList;
import cisneros.spectrumdemo.backend.User;

public class UserRecyclerPresenter {

    private ArrayList<User> users;

    public UserRecyclerPresenter(ArrayList<User> users) {
        this.users = users;
    }

    public void onBindUserViewAtPosition(int position, UserItemUpdater updater) { // when the adapter grabs an item at it's current position, this is called.
        User user = this.users.get(position);
        updater.setUsername(user.getUsername());
        updater.setFirstName(user.getFirstName());
        updater.setLastName(user.getLastName());
        updater.setPhoneNumber(user.getPhoneNumber());
        updater.setEmail(user.getEmail());
    }

    public int getUserCount() {
        return this.users.size();
    }

    public void updateUsers(ArrayList<User> users) {
        this.users = users;
    }

}
