package cisneros.spectrumdemo.newUser;

import android.content.Context;

public interface NewUserActivityUpdater {

    void killActivity();

    void displayUsernameError(int errorMsg);

    void displayPasswordError(int errorMsg);

    void displayFirstNameError(int errorMsg);

    void displayLastNameError(int errorMsg);

    void displayPhoneNumberError(int errorMsg);

    void displayEmailError(int errorMsg);

    void displaySnackbarMessage(int errorMsg, int length);

    Context getContext();
}
