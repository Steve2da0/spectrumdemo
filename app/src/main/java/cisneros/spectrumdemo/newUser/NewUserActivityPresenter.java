package cisneros.spectrumdemo.newUser;

import android.util.Log;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import cisneros.spectrumdemo.R;
import cisneros.spectrumdemo.backend.DatabaseHelper;

public class NewUserActivityPresenter {

    NewUserActivityUpdater updater;

    public NewUserActivityPresenter(NewUserActivityUpdater updater) { this.updater = updater; }

    public void submitButtonClicked(String username, String password, String firstName, String lastName, String phoneNumber, String email) {

        if(!validateUsername(username)) {
            return;
        }

        if(!validatePassword(password)) {
            return;
        }

        if(!validateUserInformation(firstName, lastName, phoneNumber, email)) {
            return;
        }

        boolean wasInserted = DatabaseHelper.getInstance(updater.getContext()).insertData(username, password, firstName, lastName, phoneNumber, email);
        if (wasInserted) {
            updater.killActivity();
        }

    }

    private boolean containsWhiteSpaces(String string) {
        if (!string.matches("\\S+")) {
            return true;
        }
        return false;
    }

    private boolean validateUsername(String username) {
        if (username.isEmpty()) {
            updater.displayUsernameError(R.string.empty_username);
            return false;
        }
        if (containsWhiteSpaces(username)) {
            updater.displayUsernameError(R.string.username_contains_spaces);
            return false;
        }
        if (!uniqueUsername(username)) {
            updater.displayUsernameError(R.string.username_exists);
            return false;
        }
        return true;
    }

    private boolean validatePassword(String password) {
        if (password.isEmpty()) {
            updater.displayPasswordError(R.string.empty_password);
            return false;
        }
        if (containsWhiteSpaces(password)) {
            updater.displayPasswordError(R.string.password_contains_spaces);
            return false;
        }
        if (!passwordLengthAccepatable(password)) {
            updater.displayPasswordError(R.string.invalid_password_length);
            return false;
        }

        if (!containsOnlyAlphanumeric(password)) {
            updater.displayPasswordError(R.string.non_alphaNum_password);
            return false;
        }

        if (containsSequences(password)) {
            updater.displayPasswordError(R.string.password_contains_seq);
            return false;
        }
        return true;
    }

    private boolean validateUserInformation(String firstName, String lastName, String phoneNumber, String email) {

        // does Validations for First Name
        if (firstName.isEmpty()) {
            updater.displayFirstNameError(R.string.empty_firstName);
            return false;
        }
        if (containsWhiteSpaces(firstName)) {
            updater.displayFirstNameError(R.string.firstName_contains_spaces);
            return false;
        }
        if (!isOnlyAlphaLetters(firstName)) {
            updater.displayFirstNameError(R.string.firstName_contains_nonAlphabetic);
            return false;
        }

        // does Validations for Last Name
        if (lastName.isEmpty()) {
            updater.displayLastNameError(R.string.empty_lastName);
            return false;
        }
        if (containsWhiteSpaces(lastName)) {
            updater.displayLastNameError(R.string.lastName_contains_spaces);
            return false;
        }
        if (!isOnlyAlphaLetters(lastName)) {
            updater.displayLastNameError(R.string.lastName_contains_nonAlphabetic);
            return false;
        }

        // does Validation for Phone Number
        if (phoneNumber.isEmpty()) {
            updater.displayPhoneNumberError(R.string.empty_phoneNumber);
            return false;
        }
        if (!isOnlyNumbers(phoneNumber)) {
            updater.displayPhoneNumberError(R.string.phoneNumber_contains_nonInteger);
            return false;
        }

        if (!phoneNumberLength(phoneNumber)) {
            updater.displayPhoneNumberError(R.string.invalid_phoneNumber_length);
            return false;
        }

        // does Validation for Email
        if (email.isEmpty()) {
            updater.displayEmailError(R.string.empty_email);
            return false;
        }
        if (containsWhiteSpaces(email)){
            updater.displayEmailError(R.string.email_contains_spaces);
            return false;
        }
        if (!isEmail(email)) {
            updater.displayEmailError(R.string.invalid_email);
            return false;
        }
        return true;
    }

    private boolean isEmail(String email) {

        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true;
        }
        return false;
    }

    private boolean isOnlyAlphaLetters(String name) {
        if (name.matches("[a-zA-Z]+")) return true;
        else return false;
    }

    private boolean isOnlyNumbers(String name) {
        if (name.matches("[0-9]+")) return true;
        else return false;
    }

    private boolean containsOnlyAlphanumeric(String password) {
        if(password.matches("[a-zA-Z0-9]*")) {
            return true;
        }
        return false;
    }

    private boolean phoneNumberLength(String phoneNumber) {
        if (phoneNumber.length() != 10) {
            return false;
        }
        return true;
    }

    private boolean passwordLengthAccepatable(String password) {
        if (password.length()>=5 && password.length()<=12)
            return true;
        else
            return false;
    }

    private boolean containsSequences(String password) {
        Matcher sequence = Pattern.compile("(\\w{2,})\\1").matcher(password);
        if (sequence.find()) {
            return true;
        }
        return false;
    }


    private boolean uniqueUsername(String username) {
        return DatabaseHelper.getInstance(updater.getContext()).uniqueUsername(username);
    }


}
