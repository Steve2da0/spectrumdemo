package cisneros.spectrumdemo.backend;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper sInstance;

    public static synchronized DatabaseHelper getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    private static final String DATABASE_NAME = "Users.db";
    private static final int DATABASE_VERSION = 1;


    // this table represents a group of users who were added from an individual user
    private static final String GROUP_TABLE = "group_table";
    private static final String GROUP_ID = "group_id";

    private static final String CREATE_GROUP_TABLE = "CREATE TABLE " + GROUP_TABLE + "("
            + GROUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT "
            + ");";



    // this table represents all users within the application
    private static final String USERS_TABLE = "users_table";
    private static final String USER_ID = "user_id";
    private static final String USER_NAME = "username";
    private static final String PASSWORD = "password";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String PHONE_NUMBER = "phone_number";
    private static final String EMAIL = "email";

    private static final String CREATE_USERS_TABLE = "CREATE TABLE " + USERS_TABLE + "("
            + USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + USER_NAME + " TEXT NOT NULL, "
            + PASSWORD + " TEXT NOT NULL, "
            + FIRST_NAME + " TEXT NOT NULL, "
            + LAST_NAME + " TEXT NOT NULL, "
            + PHONE_NUMBER + " TEXT NOT NULL, "
            + EMAIL + " TEXT NOT NULL "
            + ");";




    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_GROUP_TABLE);
        sqLiteDatabase.execSQL(CREATE_USERS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + USERS_TABLE);
        onCreate(sqLiteDatabase);
    }

    public boolean insertData(String username, String password, String firstName, String lastName, String phoneNumber, String email) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_NAME, username);
        contentValues.put(PASSWORD, password);
        contentValues.put(FIRST_NAME, firstName);
        contentValues.put(LAST_NAME, lastName);
        contentValues.put(PHONE_NUMBER, phoneNumber);
        contentValues.put(EMAIL, email);
        long inserted = db.insert(USERS_TABLE, null, contentValues);

        /**
         * SQLiteDatabase insert method returns -1 if the data was not able to be inserted,
         * otherwise return long value of data
         */

        if (inserted == -1) {
            return false;
        }
        return true;
    }

    public ArrayList<User> getAllUsers() {
        ArrayList<User>allUsers = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + USERS_TABLE, null);
            if (cursor.getCount() == 0) {
                cursor.close();
                return null;
            }

            while(cursor.moveToNext()) {
                User user = new User(cursor.getString(1),cursor.getString(2),cursor.getString(3),
                        cursor.getString(4),cursor.getString(5),cursor.getString(6));
                allUsers.add(user);
            }
            cursor.close();
        return allUsers;
    }

    private boolean databaseEmpty() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM "+USERS_TABLE;
        Cursor cursor = db.rawQuery(count, null);
        cursor.moveToFirst();
        int icount = cursor.getInt(0);
        if(icount>0) {
            cursor.close();
            return false;
        }
        else {
            cursor.close();
            return true;
        }
    }

    public boolean uniqueUsername(String username) {
        if (databaseEmpty()) {
            return true;
        } else {
            String Query = "SELECT * FROM " + USERS_TABLE + " WHERE " + USER_NAME + " = '" +  username + "'";
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() <= 0){
                cursor.close();
                return true;
            }
            cursor.close();
            return false;
        }
    }

    public boolean creditentialsMatch(String username, String password) {
        String Query = "SELECT * FROM " + USERS_TABLE + " WHERE " + USER_NAME + " = '" +  username + "' " +
                "AND " + PASSWORD + " = '" +  password +"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

}
