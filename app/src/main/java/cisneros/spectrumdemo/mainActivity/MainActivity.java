package cisneros.spectrumdemo.mainActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cisneros.spectrumdemo.backend.User;
import cisneros.spectrumdemo.R;
import cisneros.spectrumdemo.login.LoginActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainActivityUpdater {

    private MainActivityPresenter presenter;
    private RecyclerView userRecyclerView;
    private UserRecyclerAdapter userRecyclerAdapter;
    private UserRecyclerPresenter userRecyclerPresenter;
    private ProgressBar progressBar;
    private FloatingActionButton logOutButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.presenter = new MainActivityPresenter(this);

        setContentView(R.layout.activity_main);

        this.userRecyclerView = findViewById(R.id.recycler_view);
        this.progressBar = findViewById(R.id.progress_bar);
        this.logOutButton = findViewById(R.id.user_logout);

        this.userRecyclerPresenter = new UserRecyclerPresenter(presenter.retrieveUsers());
        this.userRecyclerAdapter = new UserRecyclerAdapter(userRecyclerPresenter);
        this.userRecyclerView.setAdapter(userRecyclerAdapter);
        this.userRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.retrieveUsers();
    }

    @Override
    public void displayProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void updateUsers(final ArrayList<User> users) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                userRecyclerPresenter.updateUsers(users);
                userRecyclerAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

}
