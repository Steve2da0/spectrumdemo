package cisneros.spectrumdemo.mainActivity;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import cisneros.spectrumdemo.R;

public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerViewHolder> {

    private final UserRecyclerPresenter userRecyclerPresenter;

    public UserRecyclerAdapter(UserRecyclerPresenter presenter) {
        this.userRecyclerPresenter = presenter;
    }

    @NonNull
    @Override
    public UserRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserRecyclerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.new_user_item,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UserRecyclerViewHolder holder, int position) {
        this.userRecyclerPresenter.onBindUserViewAtPosition(position, holder);
    }

    @Override
    public int getItemCount() {
        return this.userRecyclerPresenter.getUserCount();
    }
}
