package cisneros.spectrumdemo.login;

import com.google.android.material.snackbar.Snackbar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import cisneros.spectrumdemo.R;
import cisneros.spectrumdemo.backend.DatabaseHelper;

public class LoginActivityPresenter {

    LoginActivityUpdater updater;
    LoginActivityPresenter(LoginActivityUpdater updater) {
        this.updater = updater;
    }

    public void loginButtonClicked(String username, String password) {
        boolean usernamePassed = validateUsername(username);
       boolean passwordPassed =  validatePassword(password);
       boolean match = credentialsMatch(username, password);
       if (usernamePassed && passwordPassed) {
           if (!match) {
               updater.displaySnackbar(R.string.non_matching, Snackbar.LENGTH_LONG);
           }
           else
           updater.launchMainActivity();
       }
    }

    public void signUpButtonClicked() {
        updater.launchNewUserActivity();
    }

    private boolean containsWhiteSpaces(String string) {
        if (!string.matches("\\S+")) {
            return true;
        }
        return false;
    }

    private boolean validateUsername(String username) {
        if (username.isEmpty()) {
            updater.displayUsernameError(R.string.empty_username);
            return false;
        }
        if (containsWhiteSpaces(username)) {
            updater.displayUsernameError(R.string.username_contains_spaces);
            return false;
        }

        return true;
    }

    private boolean validatePassword(String password) {
        if (password.isEmpty()) {
            updater.displayPasswordError(R.string.empty_password);
            return false;
        }
        if (containsWhiteSpaces(password)) {
            updater.displayPasswordError(R.string.password_contains_spaces);
            return false;
        }
        if (!passwordLengthAccepatable(password)) {
            updater.displayPasswordError(R.string.invalid_password_length);
            return false;
        }

        if (!containsOnlyAlphanumeric(password)) {
            updater.displaySnackbar(R.string.non_alphaNum_password, Snackbar.LENGTH_LONG);
            return false;
        }

        if (containsSequences(password)) {
            updater.displaySnackbar(R.string.password_contains_seq, Snackbar.LENGTH_LONG);
            return false;
        }
        return true;
    }

    private boolean containsOnlyAlphanumeric(String password) {
        if(password.matches("[a-zA-Z0-9]*")) {
            return true;
        }
        return false;
    }

    private boolean passwordLengthAccepatable(String password) {
        if (password.length()>=5 && password.length()<=12)
            return true;
        else
            return false;
    }

    private boolean containsSequences(String password) {
        Matcher sequence = Pattern.compile("(\\w{2,})\\1").matcher(password);
        if (sequence.find()) {
            return true;
        }
        return false;
    }

    private boolean credentialsMatch(String username, String password) {
        return DatabaseHelper.getInstance(updater.getContext()).creditentialsMatch(username, password);
    }

}
