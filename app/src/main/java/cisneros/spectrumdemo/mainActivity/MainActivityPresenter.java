package cisneros.spectrumdemo.mainActivity;

import java.util.ArrayList;

import cisneros.spectrumdemo.backend.DatabaseHelper;
import cisneros.spectrumdemo.backend.User;

public class MainActivityPresenter {

    private MainActivityUpdater updater;

    public MainActivityPresenter(MainActivityUpdater updater) {
        this.updater = updater;
    }

    public ArrayList<User> retrieveUsers() {
       return DatabaseHelper.getInstance(updater.getContext()).getAllUsers();
    }

}
