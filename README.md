# Spectrum Demo

This Android Application was created to show case an application that fullfills all the requirements in the demo app sheet.
This application allows users to create an account that is added to a list of users existing in a database.
It also allows you to login with the account information submitted from creating an account.
This application follows the MVP pattern.

## Getting Started

To use this application you can download the application SDK directly onto your device from the shared Google Drive link below.

https://drive.google.com/open?id=1rVuSsBwcX7onv2HBCXxiFaFz5SaZ7RDW

Another form of downloading the application is cloning the repository and running the application via Android Studio.

git clone https://Steve2da0@bitbucket.org/Steve2da0/spectrumdemo.git

### Prerequisites

IDE compatible with Android (Android Studio).


## Built With

* Android Studio
* SQLite3

## Versioning
* Version 1.0

## Authors
* Steven Cisneros

