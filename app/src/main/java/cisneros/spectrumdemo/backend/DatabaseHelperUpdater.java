package cisneros.spectrumdemo.backend;

public interface DatabaseHelperUpdater {

    public boolean insertData(String username, String password, String firstName,
                              String lastName, String address, String city, String state, String zipCode, String phoneNumber, String email);

}
