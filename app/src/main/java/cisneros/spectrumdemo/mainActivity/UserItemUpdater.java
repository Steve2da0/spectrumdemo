package cisneros.spectrumdemo.mainActivity;

/**
 * Updates the individual items in the RecyclerView
 */
public interface UserItemUpdater {

    void setUsername(String username);

    void setFirstName(String firstName);

    void setLastName(String lastName);

    void setPhoneNumber(String phoneNumber);

    void setEmail(String email);
}
